class Chat extends React.Component {
  wrapperRef = React.createRef();

  componentDidMount() {
    this.wrapperRef.current.scrollTop = this.wrapperRef.current.scrollHeight;
  }

  getSnapshotBeforeUpdate(prevProps, prevState) {
    const wrapper = this.wrapperRef.current;
    return wrapper.scrollTop + wrapper.offsetHeight >= wrapper.scrollHeight;
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (snapshot) {
      this.wrapperRef.current.scrollTop = this.wrapperRef.current.scrollHeight;
    }
  }


  render() {
    return (
        <div
            style={{
              height: 200,
              overflowY: "scroll",
              border: "1px solid #ccc"
            }}
            ref={this.wrapperRef}
            children={this.props.children}
        >
          {this.props.children}
        </div>
    );
  }
}



class App extends React.Component {
  state = {
    messages: [
      "Message","Message","Message"
    ]
  };

  addMessage = () => {
    this.setState(state => {
      return {
        messages: state.messages.concat(["Message"])
      };
    });
  };

  render() {
    return (
        <div>
          <h3>#Exam4 - New lifecycle methods</h3>
          <Chat>
            {this.state.messages.map((message,index) => (
                <div key={index}>
                  {`${message}-${index}`}
                </div>
            ))}
          </Chat>
          <button onClick={this.addMessage}>Add</button>
        </div>
    );
  }
}

ReactDOM.render(<App/>, document.getElementById('root'));