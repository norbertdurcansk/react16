const ButtonContext = React.createContext("dark");

const Button = ({children}) => {


  return (
      <ButtonContext.Consumer>
        {(type) => {
          const style = type === "dark" ? {
                background: "#666"
              } :
              {
                background: "#eee"
              };
          return (<button
              style={style}>
            {children}
          </button>)
        }
        }
      </ButtonContext.Consumer>
  )
};

class App extends React.Component {

  render() {
    return (
        <div>
          <h3>Context API</h3>
          <ButtonContext.Provider value="dark">
            <Button>
              I am button one
            </Button>
          </ButtonContext.Provider>
          <ButtonContext.Provider value="light">
            <Button>
              I am button two
            </Button>
          </ButtonContext.Provider>
        </div>
    );
  }
}


ReactDOM.render(
    <App/>
    , document.getElementById('root'));