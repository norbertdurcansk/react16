class ErrorHandler extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hasError: false,
    }
  }

  componentDidCatch(error, info) {
    this.setState({hasError: true});
  }

  render() {

    return this.state.hasError ? <div>Error has occurred!</div>
        : this.props.children


  }
}

const User = ({user}) => (
    <div>I am {user.name}</div>
);


class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {
        name: "Norbert",
        surname: "-"
      }
    };
  }

  updateUser = () => {
    this.setState({user: null});
  };

  render() {
    return (
        <div>
          <h3>#Exam1 - componentDidCatch</h3>
          <ErrorHandler>
            <User user={this.state.user}/>
            <button onClick={this.updateUser}>Remove user</button>
          </ErrorHandler>
        </div>
    );
  }
}

ReactDOM.render(
    <App/>
    , document.getElementById("root"));
