class City extends React.Component {
  constructor(props) {
    super(props);
    this.state = {loading: true};
    setTimeout(() => {
      this.setState({loading: false});
    }, 1000);
  }

  componentWillReceiveProps() {
    this.setState({loading: true});
    setTimeout(() => {
      this.setState({loading: false});
    }, 1000);
  }

  render() {
    if (this.state.loading) {
      return "loading";
    }
    return this.props.name;
  }
}

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      city: 'Brno'
    };
  }

  selectCity = evt => {
    const {target: {value}} = evt;
    this.setState(({city}) => city === value ? null : {city: value})
  };

  render() {
    return (
        <div>
          <h3>#Exam3 - Set state</h3>
          <button
              type="button"
              value="brno"
              onClick={this.selectCity}>
            Brno
          </button>
          <button
              type="button"
              value="praha"
              onClick={this.selectCity}>
            Praha
          </button>
          <button
              type="button"
              value="kosice"
              onClick={this.selectCity}>
            Kosice
          </button>
          <City name={this.state.city}/>
        </div>
    );
  }
}


ReactDOM.render(<App/>, document.getElementById('root'));
