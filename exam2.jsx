const SomeItems = () => [
      <li key="1">Apple</li>,
      <li key="2">Orange</li>,
      <li key="3">Banana</li>,
];

const Text = () => "Footer message"



const MoreItems = () => [
  <li key="1">
    New Item 1
  </li>,
  <li key="2">
    New Item 2
  </li>
];

class App extends React.Component {
  render() {
    return (
        <div>
          <h3>#Exam2 - Components array, text component</h3>
          <ul>
            <li>Item 1</li>
            <li>Item 2</li>
            <SomeItems/>
            <MoreItems/>
            <Text/>
          </ul>
        </div>
    );
  }
}

ReactDOM.render(<App/>, document.getElementById('root'));
